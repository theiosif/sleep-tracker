# -*- coding: utf-8 -*-
'''setup.py'''
from setuptools import setup, find_packages


setup(name='Pasithea',
      packages=find_packages(),
      install_requires=[
          'nose',
          'coverage',
          'numpy',
          'scipy',
          'matplotlib',
          'pandas',
          # 'os',
          # 'struct',
          # 'calendar',
          # 'csv',
          # 'tkinter',
          'Pillow'
          # 'pylab'

          # Add your project dependencies to this list, e.g. pyaudio, pygame,
          # ...
      ]  # ,
      # entry_points={'console_scripts': [
      # Add the functions that should become binaries here
      #  'myhello = example_package.example_module:example_function', ]}
      )
