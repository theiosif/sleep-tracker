# -*- coding: utf-8 -*-
"""
Created on Mon Jul  2 17:43:03 2018

@author: Eftihia
"""
import pandas as pd
import numpy as np
import tkinter
from tkinter import Label, Button, Frame, StringVar, OptionMenu
from tkinter import *
import tkinter as tk
import tkinter.messagebox
import matplotlib.pyplot as plt
from PIL import Image, ImageTk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.backends.backend_tkagg import NavigationToolbar2TkAgg
import os
import matplotlib
matplotlib.use("TkAgg")

LARGE_FONT = ("Helvetica", 12)
COLOUR = ("#2EC89D")

FAKE = False

if FAKE:
    CSVFILE = ('AS_FAKE_AS_IT_CAN_GET.csv')
else:
    CSVFILE = ('database.csv')


# In[]:

database = pd.read_csv(CSVFILE)
for index, row in database.iterrows():
    row['Stage_Start'] = row['Stage_Start'].split()
    row['Stage_Stop'] = row['Stage_Stop'].split()

num_entries = len(database)

# In[]:
ROW_BAK = database.iloc[0]
# the_problem = ROW_BAK['Stage_Start']


def fix_date(date):
    date = str(date)
    date = date[:-4]
    date = pd.to_datetime(date, format="%Y%m%d")
    return date


def fix_times(the_problem):
    # we want a uniform format on all strings
    the_problem = the_problem[1:-1]+','
    the_problem = the_problem.split()
    the_problem = pd.to_datetime(the_problem, format="'%Y%m%d%H%M',")

    the_solution = the_problem
    return the_solution


def fix_list(strings_list):
    strings_list = strings_list[1:-1]
    strings_list = strings_list.split(', ')
    int_list = list(map(int, strings_list))
    return int_list


# In[]:


def import_db(filename):
    database = pd.read_csv(filename)

    num_entries = len(database)

    database['Date'] = [fix_date(x) for x in database['Date']]
    database['Stage_Start'] = [fix_times(x) for x in database['Stage_Start']]
    database['Stage_Stop'] = [fix_times(x) for x in database['Stage_Stop']]
    database['Stage_Type'] = [fix_list(x) for x in database['Stage_Type']]
    database['Total_Time'] = database['Total_Time']/3600

    return num_entries, database


n, getmydata = import_db(CSVFILE)

# In[]:


def plot_sleep_stages(database, day_no):
    f = plt.figure()
    day = database.iloc[day_no - 1]
    day_name = database['Date'][day_no - 1]
    plt.suptitle('Sleep Stages on {}'.format(day_name))
    plt.plot(day['Stage_Start'], day['Stage_Type'], 'r-o')
    plt.yticks(np.arange(4), ['Deep', 'REM', 'Light', 'Wake'])
    return f
# In[]:


def plot_sleep_scores(day_no, database):
    f = plt.figure()
    day_start = database['Date'][day_no - 7]
    day_stop = database['Date'][day_no - 1]
    plt.suptitle('Sleep Quality from {} to {}'.format(day_start, day_stop))
    plt.xlabel('Date')
    plt.ylabel('Sleep Quality')
    plt.plot(database['Date'], database['Score'], 'r-o')
    return f
# In[]:


def plot_sleep_time(day_no, database):
    f = plt.figure()
    day_start = database['Date'][day_no - 7]
    day_stop = database['Date'][day_no - 1]
    plt.suptitle(
            'Total Sleep Time (hrs) from {} to {}'.format(day_start, day_stop))
    plt.xlabel('Date')
    plt.ylabel('Sleep Time')
    plt.plot(database['Date'], database['Total_Time'], 'g-v')
    return f

# In[]:


def showImg(self):
        load = Image.open("img.gif")
        render = ImageTk.PhotoImage(load)
        # labels can be text or images
        img = Label(self, image=render)
        img.image = render
        img.place(x=0, y=0)

# In[]:


def namePage(name_pg, name_button):
    print('The page: %s' % name_pg)
    print('The button: %s' % name_button)

# In[]


def variable_changed(current_opt, *evt):
    print("Variable changed:", current_opt.get(), evt)

# In[]:


def SearchNewFiles():
    import os
    from dataparse.data_parser import parse_func as parse
    from accelerometer.calculator_angle import total_result as acc
    import heartrate as hr
    from heartrate.heartrate import process_hr_data as pro_hr
    hr.TESTING = False
    hr.READFROMFILE = False

    file_content = os.listdir("files")
    os.chdir("files")
    file_content.sort()
    file_content.remove('.gitkeep')
    file_content.remove('processed')
    if len(file_content) > 0:
        a = 1
    else:
        a = 0

    for file in file_content:
        print('Processing File: ', file)
        parsed = parse(file)
        if len(parsed[1]) > 100:
            pro_acc = acc(parsed[0], parsed[1])
            processed = pro_hr(pro_acc[1], parsed[1])
            os.chdir("..")
            hr.heartrate.make_my_day(processed)
            os.chdir("files")
        os.rename(file, 'processed/'+file)
    os.chdir("..")

    return a


# In[]:

PROJECT_DIR = os.path.dirname(os.path.abspath('GUI'))


def _abspath(name):
    return os.path.abspath(os.path.join(PROJECT_DIR, name))


def increment(a):
    a = a+1
    print(a)
# In[]:


class Application(tk.Tk):

    def __init__(self, *args, **kwargs):

        tk.Tk.__init__(self, *args, **kwargs)
        tk.Tk.wm_title(self, "Huawei Smartwatch")

        container = tk.Frame(self)
        container.pack(side='top', fill='both', expand=True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        # menu bar
        menubar = tk.Menu(container)
        filemenu = tk.Menu(menubar, tearoff=0)
        filemenu.add_separator()
        filemenu.add_command(label="Exit", command=self.quit)
        # add_cascade to menubar
        menubar.add_cascade(label="File", menu=filemenu)
        graph = tk.Menu(menubar, tearoff=1)
        graph.add_command(label="Graphs",
                          command=lambda: self.show_frame(GraphPage))
        graph.add_command(label="Sleep Stages",
                          command=lambda: self.show_frame(SleepStages))
        graph.add_command(label="Sleept Scores",
                          command=lambda: self.show_frame(SleepScores))
        graph.add_command(label="Sleep Time",
                          command=lambda: self.show_frame(SleepTime))
        menubar.add_cascade(label="Graphs", menu=graph)
        tk.Tk.config(self, menu=menubar)

    #    instead of looping over a list of classes,
    #    we make a simple loop

        self.framesDay = {}
        for i in range(1, 31):
            self.framesDay['i'] = i
        self.frames = {}

        Pages = (StartPage, GraphPage,
                 SleepStages, SleepScores, SleepTime)
        # for frames already decleared
        for F in Pages:
            frame1 = F(container, self)
            self.frames[F] = frame1
            frame1.grid(row=0, column=0, sticky="nsew")

#        self.days = []
#        for index in range(31):
#            day = Day(container, self, index)
#            self.days.append(day)
#            day.grid(row=0, column=0, sticky='nsew')

        self.show_frame(StartPage)

    def show_frame(self, cont):
        frame1 = self.frames[cont]
        result = print(cont)
        frame1.tkraise()
        return result

    def check_files(self):
        from tkinter import messagebox
        r = SearchNewFiles()
        if r == 1:
            messagebox.showinfo("Checked!", "New files added in database!")
        elif r == 0:
            messagebox.showwarning("Checked!", "No new files imported!")

    def search_NewFilesButt(event):
        print('The <<Search new files buttons has been accessed: >>',
              event.x, event.y)


class StartPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        showImg(self)

        button0 = tk.Button(self, text="Check New Files", width=25,
                            bg=COLOUR, font=LARGE_FONT,
                            command=lambda: controller.check_files())
        button0.pack()
        button0.place(x=310, y=175)
        a = 0
        increment(a)

        button = tk.Button(self, text="Graph Page", width=25, bg=COLOUR,
                           font=LARGE_FONT,
                           command=lambda: controller.show_frame(GraphPage))
        button.pack()
        button.place(x=310, y=225)
        button.invoke()
        button.bind('Button', namePage('GraphPage', 'Button1'))

        button2 = tk.Button(self, text="Exit", width=25, bg="#2EC89D",
                            font=LARGE_FONT,
                            command=lambda: controller.destroy())
        button2.pack()
        button2.place(x=310, y=275)


class GraphPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        showImg(self)
        a = 1
        increment(a)

        button3 = tk.Button(self, text="Sleep Stages", width=25, bg=COLOUR,
                            font=LARGE_FONT,
                            command=lambda: controller.show_frame(SleepStages))
        button3.pack()
        button3.place(x=310, y=150)

        button4 = tk.Button(self, text="Sleep Scores", width=25, bg=COLOUR,
                            font=LARGE_FONT,
                            command=lambda: controller.show_frame(SleepScores))
        button4.pack()
        button4.place(x=310, y=200)

        button4 = tk.Button(self, text="Sleep Time", width=25, bg=COLOUR,
                            font=LARGE_FONT,
                            command=lambda: controller.show_frame(SleepTime))
        button4.pack()
        button4.place(x=310, y=250)

        button = tk.Button(self, text="Back Home", width=25, bg=COLOUR,
                           font=LARGE_FONT,
                           command=lambda: controller.show_frame(StartPage))
        button.pack()
        button.place(x=310, y=300)


class SleepStages(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        showImg(self)
        self.createOM()

        self.parent = parent
        button = tk.Button(self, text="Back to Graph Page", width=22,
                           bg=COLOUR,
                           font=LARGE_FONT,
                           command=lambda: controller.show_frame(GraphPage))
        button.grid(row=2, column=0)

        button1 = tk.Button(self, text="Back Home", width=22, bg=COLOUR,
                            font=LARGE_FONT,
                            command=lambda: controller.show_frame(StartPage))
        button1.grid(row=3, column=0)

#        x = plot_sleep_stages(getmydata, 1)
#        canvas = FigureCanvasTkAgg(x, self)
#        canvas.show()
#        canvas.get_tk_widget().grid(row=1, column=3,
#                                    sticky='n')
#
#        toolbar = NavigationToolbar2TkAgg(canvas, self)
#        toolbar.update()
#        canvas._tkcanvas.grid(row=1, column=2)

    def createOM(self):
        # Create OptionMenu
        omlist = ['Select Day']
        self.var = StringVar()
        self.options = OptionMenu(self, self.var, *omlist,
                                  command=self.Canvas)
        self.options.config(bg=COLOUR)
        self.options.grid(row=4, column=0)

        values = ['1', '2', '3', '4', '5', '6', '7',
                  '8', '9', '10', '11', '12', '13', '14', '15', '16',
                  '17', '18', '19', '20', '21', '22', '23', '24', '25',
                  '26', '27', '28', '29']
        for v in values:
            self.update()
            self.options['menu'].add_command(
                label=v, command=_setit(self.var, v, self.Canvas))

    def Canvas(self, value, *args):
        nr = int(value)
        print(value)
        x = plot_sleep_stages(getmydata, nr)
        canvas = FigureCanvasTkAgg(x, self)
        canvas.show()
        canvas.get_tk_widget().grid(row=0, column=2, rowspan=200,
                                    columnspan=40)

        toolbar = NavigationToolbar2TkAgg(canvas, self)
        toolbar.update()
        canvas._tkcanvas.grid(row=1, column=2, rowspan=1, columnspan=1)


class SleepScores(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        showImg(self)
        self.createOM()
        self.grid()

        self.parent = parent

        button = tk.Button(self, text="Back to Graph Page", width=22,
                           bg=COLOUR,
                           font=LARGE_FONT,
                           command=lambda: controller.show_frame(GraphPage))
        button.grid(row=1, column=0)

        button1 = tk.Button(self, text="Back Home", width=22, bg=COLOUR,
                            font=LARGE_FONT,
                            command=lambda: controller.show_frame(StartPage))
        button1.grid(row=2, column=0)

    def createOM(self):
        # Create OptionMenu
        omlist = ['Select Week']
        self.var = StringVar()
        self.options = OptionMenu(self, self.var, *omlist,
                                  command=self.Canvas)
        self.options.grid(row=4, column=0)
        self.options.config(bg=COLOUR)

        values = ['Week 1', 'Week 2', 'Week 3', 'Week 4']
        for v in values:
            self.options['menu'].add_command(
                label=v, command=_setit(self.var, v, self.Canvas))

    def Canvas(self, value, *args):

        nr = str(value)
        print(nr)
        if nr == 'Week 1':
            week = 7
            print(week)
            x = plot_sleep_scores(week, getmydata)
            canvas = FigureCanvasTkAgg(x, self)
            canvas.show()
            canvas.get_tk_widget().grid(row=0, column=2, rowspan=200,
                                        columnspan=40)

            toolbar = NavigationToolbar2TkAgg(canvas, self)
            toolbar.update()
            canvas._tkcanvas.grid(row=1, column=2, rowspan=1, columnspan=1)

        elif nr == 'Week 2':
            week = 14
            x = plot_sleep_scores(week, getmydata)
            canvas = FigureCanvasTkAgg(x, self)
            canvas.show()
            canvas.get_tk_widget().grid(row=0, column=2, rowspan=200,
                                        columnspan=40)
            toolbar = NavigationToolbar2TkAgg(canvas, self)
            toolbar.update()
            canvas._tkcanvas.grid(row=1, column=2, rowspan=1, columnspan=1)

        elif nr == 'Week 3':
            week = 21
            x = plot_sleep_scores(week, getmydata)
            canvas = FigureCanvasTkAgg(x, self)
            canvas.show()
            canvas.get_tk_widget().grid(row=0, column=2, rowspan=200,
                                        columnspan=40)
            toolbar = NavigationToolbar2TkAgg(canvas, self)
            toolbar.update()
            canvas._tkcanvas.grid(row=1, column=2, rowspan=1, columnspan=1)

        elif nr == 'Week 4':
            week = 28
            x = plot_sleep_scores(week, getmydata)
            canvas = FigureCanvasTkAgg(x, self)
            canvas.show()
            canvas.get_tk_widget().grid(row=0, column=2, rowspan=200,
                                        columnspan=40)
            toolbar = NavigationToolbar2TkAgg(canvas, self)
            toolbar.update()
            canvas._tkcanvas.grid(row=1, column=2, rowspan=1, columnspan=1)


class SleepTime(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        showImg(self)
        self.createOM()
        self.grid()

        self.parent = parent

        button = tk.Button(self, text="Back to Graph Page", width=22,
                           bg=COLOUR,
                           font=LARGE_FONT,
                           command=lambda: controller.show_frame(GraphPage))
        button.grid(row=1, column=0)

        button1 = tk.Button(self, text="Back Home", width=22, bg=COLOUR,
                            font=LARGE_FONT,
                            command=lambda: controller.show_frame(StartPage))
        button1.grid(row=2, column=0)

    def createOM(self):
        # Create OptionMenu
        omlist = ['Select Week']
        self.var = StringVar()
        self.options = OptionMenu(self, self.var, *omlist,
                                  command=self.Canvas)
        self.options.grid(row=4, column=0)
        self.options.config(bg=COLOUR)

        values = ['Week 1', 'Week 2', 'Week 3', 'Week 4']
        for v in values:
            self.options['menu'].add_command(
                label=v, command=_setit(self.var, v, self.Canvas))

    def _print1(self, value, *args):
        # callback for OptionMenu has these arguements because inherently it
        # uses the _setit class to configure
        # the callback with these arguements.
        print()
        print(value)
        # self.var.set('option10') #Uncomment to change OptionMenu display
        print(self.var.get())

    def Canvas(self, value, *args):
        nr = str(value)
        print(nr)
        if nr == 'Week 1':
            week = 7
            print(week)
            x = plot_sleep_time(week, database)
            canvas = FigureCanvasTkAgg(x, self)
            canvas.show()
            canvas.get_tk_widget().grid(row=0, column=2, rowspan=200,
                                        columnspan=40)
            toolbar = NavigationToolbar2TkAgg(canvas, self)
            toolbar.update()
            canvas._tkcanvas.grid(row=1, column=2, rowspan=1, columnspan=1)

        elif nr == 'Week 2':
            week = 14
            x = plot_sleep_time(week, database)
            canvas = FigureCanvasTkAgg(x, self)
            canvas.show()
            canvas.get_tk_widget().grid(row=0, column=2, rowspan=200,
                                        columnspan=40)
            toolbar = NavigationToolbar2TkAgg(canvas, self)
            toolbar.update()
            canvas._tkcanvas.grid(row=1, column=2, rowspan=1, columnspan=1)

        elif nr == 'Week 3':
            week = 21
            x = plot_sleep_time(week, database)
            canvas = FigureCanvasTkAgg(x, self)
            canvas.show()
            canvas.get_tk_widget().grid(row=0, column=2, rowspan=200,
                                        columnspan=40)
            toolbar = NavigationToolbar2TkAgg(canvas, self)
            toolbar.update()
            canvas._tkcanvas.grid(row=1, column=2, rowspan=1, columnspan=1)

        elif nr == 'Week 4':
            week = 28
            x = plot_sleep_time(week, database)
            canvas = FigureCanvasTkAgg(x, self)
            canvas.show()
            canvas.get_tk_widget().grid(row=0, column=2, rowspan=200,
                                        columnspan=40)
            toolbar = NavigationToolbar2TkAgg(canvas, self)
            toolbar.update()
            canvas._tkcanvas.grid(row=1, column=2, rowspan=1, columnspan=1)


class _setit:
    """Internal class. It wraps the command in the widget OptionMenu."""
    def __init__(self, var, value, callback=None):
        self.__value = value
        self.__var = var
        self.__callback = callback

    def __call__(self, *args):
        self.__var.set(self.__value)
        if self.__callback:
            self.__callback(self.__value, *args)


def start_interface():
    app = Application()
    app.geometry("848x480")
    app.resizable(width=False, height=False)
    app.update()
    app.update_idletasks()
    app.mainloop()


if __name__ == '__main__':
    start_interface()
