# Project Plan


## Description of End Product

This application provides a user with sleep quality metrics. Usability and functions of the application are provided below, providing the basic usage scenario:

- The user can open our App on PC.
- A menu will be displayed where he can he see the possible options:
    1. sleep graphs
    2. sleep score
    3. sleep advice
    4. settings
- Further submenu description:
    - Selecting sleep graphs displays multiple ways of data visualisation through
	which the user can swipe through:
		* sleep phases per night
		* sleep quality history
		* REM time
		* % of deep sleep
    - Selecting `score` displays an estimate of how good the user is resting (in %)
    - Selecting `sleep advice` would display recommendations based on data
    - Selecting `settings` lets the user to enter personal data such as age,
		resting heart rate, desired sleeping time


## Description of the work packages

* **dataparse** - converts raw sensor data into program variables   
* **accelerometer** - further processes accelerometer data + HR data   
* **heartrate** - processes HR + acc data, generates database entries    
* **datafaker** - although not integrated in main, we used this to initially   
generate a fake database.
The aforementioned database was used to write the visualisation functions that    
are found inside the `import_db_and_visualise.py` script.  
These functions were later integrated into the GUI.
* **py_interface** - Everything GUI-related.

## Responsibilities (before Hamid's leave)

**Kaan**: data collection, parsing and database-related code   
**Joseph**: everything related to HR data processing and interpretation   
**Berkay**: everything related to Acc data processing and interpretation   
**Efia**: Graphical User Interface   
**Hamidreza**: processed data visualisation and processing for DB operations   


## Risks

* Bad sensor data, irregularities throughout the database   
    **this one kind of happened, since there's more than one file per day**
    **irregularities in sensor measurement some file contains no heartrate data at all**
    
        
* Achieving clearly off-mark results from data interpretation (80% REM sleep/ equiv.)   
    **this one happened**   
        
* Achieving improper functionality in VirtualEnv    
* Forgetfully omitting test-cases   


## Milestones and Timeplan

[X] marks signigy completion   
[  ] marks mean we're behind on the task   
[v] marks are leftover stuff that got done   

WEEK1:
* obtain watch connectivity and collect data [X]
* get started with understanding basic pandas, numpy, scipy [X]
* split tasks [ ]
* do sleep tracking research [X]   
   
WEEK2:
* split tasks [v]
* parse raw data [X]
* basic data processing [X]   
   
WEEK3:   
* data interpretation [ ]
* data visualisation [ ]
* GUI skeleton should be done by now [X]
* data base-related operations [X]    
   
WEEK4:   
* polishing up (debug, optimize, whatever) [X]
* finish GUI [X]
* achieve VirtualEnv functionality [X]
* begin presentation efforts [X]
* everything "mostly" works [X]

## Further details
[25.06.18] Since one member of the project unexpectedly left without any real   
contribution, we are required to update our Responsibilites and Timeplan.

# Responsibilites Redistribution
Given the obvious time limitations we are facing, no clear splitting of the tasks
can be specified.   
Database creation will be handled by Kaan and Joseph, by combining processed acc and HR   
data in a format that is easily appendable to a reasonably compact .csv file.   
Visualisation, as things stand, is handled from the GUI code. This may be subject   
to change.   

**Updates**:
* Since the GUI needed a database before we actually had a database,   
the `datafaker/fake_db_maker.py` script needed to be written.
* Visualisation utilities were written by Joseph and integrated into the  
GUI by Efia
* Main function integration was a joint effort by Kaan and Joseph.
* Virtualenv functionality was (hopefully) achieved by Joseph.  
(it works for us but we're not sure how automated testing is set up, therefore  
I left all commands I ran in the README.md)

# Timeplan Update (starting 26.06.18)
There was no timeplan update, there was just constant code writing from everyone.    
However, we dropped the `sleep advice`, as well as `settings` functionalities. 


## Project Structure, with authors
```
c-master
│   README.md
│   main.py  # (Kaan & Joseph)    
│   database_creater.py  # (Kaan, path fix by Joseph)    
│
│# Documentation folder
└───docs
│   │   projektplan.md
│   │   ProjectOverview.png  # (Joseph)
│
│# DATA PROCESSING FOR ACCELERATION
└───accelerometer
│   │   calculator_angle.py  # (Berkay)
│
│
│# UTILITY MODULE, NO INTEGRATED FUNCTIONALITY
│# Functions from the 2nd script used to visualise data
│# were copy-pasted into GUI as button functions
└───datafaker  
│   │   fake_db_maker.py  #(Joseph)
│   │   import_db_and_visualise.py  #(Joseph)
│   
│ 
│ # RAW FILE READ-IN
└───dataparse
│   │   data_parser.py  #(Kaan)
│   
│   
│ # DATA PROCESSING FOR HEARTRATE
└───heartrate
│   │   heartrate.py  #(Joseph)
│   
│
│ # GRAPHICAL USER INTERFACE
└───py_interface
│   │   py_interface.py   # (Efia)   
│   │   img.gif   # (Joseph)   
```
