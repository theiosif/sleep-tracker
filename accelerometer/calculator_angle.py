# -*- coding: utf-8 -*-
"""
Created on Sat Jun  9 23:16:36 2018

@author: Berkay
"""
import math
import numpy as np
from scipy.signal import butter, filtfilt

S_1 = [1, 2, 3]
S_2 = [6, 7, 7]
S_3 = [3, 4, 5]
S_4 = [5, 6, 2]
S_5 = [3, 4, 1]
DATA_TEST = np.concatenate([S_1, S_2, S_3, S_4, S_5], axis=0)


def calc_angle(x_cor, y_cor, z_cor):
    """calculates the angle from accelerometer datas"""
    return (math.atan(z_cor/(math.sqrt(x_cor**2+y_cor**2))))*180/math.pi


def calc_avr(values):
    """calculates the average x,y,z values over a time period"""
    return np.mean(values, axis=0)


def change_angle(a_ang, b_ang):
    """calculates the change in angle"""
    c_ang = a_ang - b_ang
    return c_ang


def butterworth_filter(cutoff, order, signal):
    """Butterworth filtering"""
    b_num, a_denom = butter(order/2, cutoff, btype='low', analog=False)
    y_out = filtfilt(b_num, a_denom, signal, axis=0)
    return y_out


def transform_array(array_to_transform, base_array):
    """adjusts the array size in order to
       make it compatible with other array
    """
    hold = len(base_array)
    if hold != 0:
        rest = len(array_to_transform) % len(base_array)
        if rest != 0:
            del array_to_transform[-rest:]
    return array_to_transform


def reshape_and_average(array_1, array_2):
    """reshapes array_1 and takes average values of every nth elements
    (with n equals to ratio)"""
    hold = len(array_2)
    if hold != 0:
        ratio = len(array_1) / len(array_2)
        array_1 = np.reshape(array_1, (int(ratio), -1), order='F')
    array_1 = calc_avr(array_1)
    return array_1


def is_there_movement(time_interval):
    """returns 1 if any movement is observed in the given time interval"""
    first_ele = time_interval[0]
    last_ele = time_interval[-1]
    angle_diff = abs(change_angle(last_ele, first_ele))
    if angle_diff < 5:
        return '0'
    elif angle_diff >= 5:
        return '1'
    return None


def total_result(acc_meas, hr_meas):
    """returns the total result of accelerometer in the following format:
    timestamp---average angle in the given time interval---1 or 0"""
    angle = []
    movement = []
    timestamp = []
    time_stamp = []
    acc_meas = transform_array(acc_meas, hr_meas)
    acc_meas = np.array(acc_meas)

    ranges = range(len(acc_meas))

    for datas in ranges:
        timestamp.append(int(acc_meas[datas][0]))
    timestamp = np.array(timestamp)
    acc_meas = butterworth_filter(0.3, 4, acc_meas[:, 1:4])

    for datas in ranges:
        angle.append(calc_angle(
            acc_meas[datas][0],
            acc_meas[datas][1],
            acc_meas[datas][2]))
    angle = reshape_and_average(angle, hr_meas)
    timestamp = reshape_and_average(timestamp, hr_meas)

    if angle.size == 1:
        time_stamp.append(timestamp)
        movement = 0.0
        return (time_stamp, movement)

    ratio_min = 300
    rest = len(angle) % ratio_min

    movement = angle[:len(angle)-rest]
    movement = np.reshape(movement, (int(ratio_min), -1), order='F')

    timestamp = timestamp[:len(timestamp)-rest]
    timestamp = np.reshape(timestamp, (int(ratio_min), -1), order='F')
    temp = 0
    ranges = timestamp[0].size
    for ind_col in range(ranges):
        temp = timestamp[:, ind_col].mean()
        time_stamp.append(int(temp))
    ind_col = 0
    for col in movement.T:
        movement[:, ind_col] = is_there_movement(movement[:, ind_col])
        col += 1
        col -= 1
        ind_col += 1
    movement = movement[0, :]
    return (time_stamp, movement)
