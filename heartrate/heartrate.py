# -*- coding: utf-8 -*-
"""
@author: Joseph

This docstring will also serve as reference for
any medical knowledge that was required.

RR interval ~= NN interval = time between 2 consec heart beats

HRV = Heart Rate Variability ~=? dRR/dt ?
    While heart rate focuses on the average beats per minute,
    HRV measures the specific changes in time
    (or variability) between successive heart beats.

===NOTICE ABOUT CODE STRUCTURE:===
[1]: Block division, although not essential, splits up the code
in its main building blocks, so that functionality can be
checked and understood with greater ease.
----
[2]: Debug Flags:
TESTING - True for function executin IN-SCRIPT
PLOT - True for visualising data at different stages
"""

# PyLint made me do this
# pylint: disable=E1101
# ^^^ This one is bc of fake 'numpy has no XXX member stuff'

from __future__ import print_function
import os
from itertools import tee
import datetime
import matplotlib.pyplot as plt
import pandas as pd
from pandas import to_datetime as todate
import numpy as np

# In[]: ## IMPORTING DATA FROM ORIGINAL MEASUREMENT

# DEBUGGING CONSTANTS
if __name__ == '__main__':
    TESTING = True
    PLOT = True
else:
    TESTING = False
    PLOT = False

# Use phony data? (was used before having a working parse function)
ITSFAKE = False

# Use accelerometer data? (was used before having accelerometer module)
ACC = True

# Export flag, used for debugging individual filereads
# as well as skipping raw file read-in for developing
EXPORT = False

# Data is very odd, we add tweaking
# parameters to get it where we want
TWEAK = 10


FILE = "2018-06-16T07_39"

if EXPORT:
    CSV_EXPORT = FILE+'_parsed.csv'

CSVFILE = FILE+'_irregular.csv'

if TESTING:
    # read from csv that contains
    # output equivalent to the
    # c/dataparse.data_parser
    # output of parse_func[1]
    # so that we don't have
    # circular imports or
    # duplicated scripts in
    # the repo.

    print("CSV DATA READ-IN")
    HRMEAS = pd.read_csv(CSVFILE)

    NUM_MEAS = len(HRMEAS)

    HRMEAS['Timestamp'] = todate(HRMEAS['Timestamp'])
    HR_DF = HRMEAS.set_index('Timestamp')

else:  # take the data from the parse function
    print("(>'.')>[hi]")


# In[]:
def hr_to_rr_milisec(instant_bpm):
    """
    converts heartrate from
    BPM to RR intervals
    """
    return 60000 / instant_bpm  # in MS


# create appropriate data structure for processing
def construct_df(measurements):
    """
    Takes raw measurement output by parser and
    creates a dataframe object with relevant
    attributes.
    """

    measurements_np = np.array(measurements)
    timestamps = measurements_np[:, 0].tolist()
    # start_time = timestamps[0]
    # stop_time = timestamps[-1]
    heart_rates = measurements_np[:, 1].tolist()

    data_frame = pd.DataFrame({'Timestamp': timestamps,
                               'BPM': heart_rates})

    data_frame['RR'] = hr_to_rr_milisec(data_frame['BPM'])

    data_frame['Timestamp'] = pd.to_datetime(data_frame['Timestamp'],
                                             unit='ms')

    data_frame.set_index('Timestamp', inplace=True)

    return data_frame  # ,start_time, stop_time


if TESTING:
    HR_DF = construct_df(HRMEAS)

if EXPORT:
    HR_DF.to_csv(CSV_EXPORT)
# In[]:


def resample_4hz(dataframe):
    """
    returns measurements interpolated
    at even sampling rate of 4Hz
    """
    heartrate_4hz = pd.DataFrame()
    heartrate_4hz['BPM'] = dataframe.BPM.resample('0.25S').mean()
    interpolated_4hz = heartrate_4hz.interpolate(method='spline', order=3)
    interpolated_4hz['RR'] = hr_to_rr_milisec(interpolated_4hz['BPM'])

    return interpolated_4hz

if TESTING:
    HR_DF = resample_4hz(HR_DF)

if PLOT:   # pragma: no cover
    # Mention about my 'pragma' approach:
    # I noticed that my cover% was small,
    # so i added the statement wherever
    # functions were either meant for
    # debugging or just wrapped predefined
    # routines from imported libraries.

    # check to see whether the thing does what it's supposed to do
    plt.figure()
    RAW_IRREG = np.array(HRMEAS)
    RAW_TIMES = RAW_IRREG[:, 0]
    RAW_IRREG = RAW_IRREG[:, 1]
    plt.plot(RAW_TIMES[500:1000], RAW_IRREG[500:1000], 'r--')
    plt.plot(HR_DF['BPM'].iloc[2500:3500], 'ko')
    plt.show()

# In[]:


def get_psd(samples_5min):   # pragma: no cover
    """
    yiels power spectral densitiy information
    """
    power_spectra = np.fft.fft(samples_5min)
    power_spectra = abs(power_spectra)**2
    frequencies = np.fft.fftfreq(1200)

    return frequencies, power_spectra


def spectral_analysis(samples_5min,
                      # vlf_low=0.003,
                      vlf=0.004,
                      lfq=0.15,
                      hfq=0.4):   # pragma: no cover
    """
    Returns relevant values:
    LF/HF ratio,
    and normalized values for
    VLF, HF and LF
    """

    # get some PSD stuff going
    freqs, pwrs = get_psd(samples_5min)

    # trim it to positive fq's
    # and take out zero fq
    freqs = freqs[TWEAK:len(freqs) // 2]
    pwrs = pwrs[TWEAK:len(pwrs) // 2]

    # Calculate the power under the given frequency band
    very_low_fq = np.trapz(y=abs(pwrs[(freqs <= vlf)]),
                           x=freqs[(freqs <= vlf)])

    low_fq = np.trapz(y=abs(pwrs[(freqs >= vlf) &
                                 (freqs <= lfq)]),
                      x=freqs[(freqs >= vlf) & (freqs <= lfq)])

    high_fq = np.trapz(y=abs(pwrs[(freqs >= lfq) &
                                  (freqs < hfq)]),
                       x=freqs[(freqs >= lfq) & (freqs < hfq)])

    total_power = np.trapz(y=abs(pwrs[(freqs >= 0) &
                                      (freqs < hfq)]),
                           x=freqs[(freqs >= 0) & (freqs < hfq)])

    # low_fq/high_fq ratio
    low_high_ratio = float(low_fq) / high_fq

    # Normalized high_fq/low_fq bands
    norm_vlf = float(very_low_fq) / total_power
    norm_hf = float(high_fq) / (total_power - very_low_fq)
    norm_lf = float(low_fq) / (total_power - very_low_fq)

    return (low_high_ratio, norm_vlf, norm_hf, norm_lf)

# In[]:


def split_df_5min(monolith_df, sample_no=1200):
    """
input - monolith_df: a Dataframe,
        sampleNo: no of rows per output df in list

output - a list of DataFrames
purpose -> splits the DataFrame into smaller of max size sampleNo
          (last is smaller)
        -> provides average BPM and RR vals for each 5min interval

    """
    list_of_df, avg_bpm, avg_rr, start_time = [], [], [], []
    arr_lo_hi_ratio, arr_vlf_norm, arr_hf_norm, arr_lf_norm = [], [], [], []
    samples_rr = []

    chunks_number = len(monolith_df) // sample_no + 1

    # create the chunks
    for i in range(chunks_number):
        samples = monolith_df[i*sample_no:(i+1)*sample_no]
        samples_rr_element = samples.RR.tolist()
        samples_rr.append(samples_rr_element)
        list_of_df.append(samples)

    # do spectral analysis and stuff on chunks
    for i in range(chunks_number-1):
        spectral_params = spectral_analysis(samples_rr[i])
        arr_lo_hi_ratio.append(spectral_params[0])
        arr_vlf_norm.append(spectral_params[1])
        arr_hf_norm.append(spectral_params[2])
        arr_lf_norm.append(spectral_params[3])

        avg_bpm.append(list_of_df[i].BPM.mean())
        avg_rr.append(list_of_df[i].RR.mean())
        start_time.append(list_of_df[i].index.values[0])

    # we drop the last one though, because otherwise
    # the Spectral analysis function throws a fit and
    # I've spent too many hours trying to find a fix
    split_data = pd.DataFrame({'startTime': start_time,
                               #  'RRpts': samples_rr[:-1],
                               'avgBPM': avg_bpm,
                               'avgRR': avg_rr,
                               'LF_HF': arr_lo_hi_ratio,
                               'LF_VLF_Norm': arr_vlf_norm,
                               'HF_Norm': arr_hf_norm,
                               'LF_Norm': arr_lf_norm,
                               'Movement': np.zeros(len(avg_bpm))})

    return split_data

# In[]:
# since we now have 5-min chunks of evenly sampled data that we constructed,
# it's time to do some FFT magic to HOPEFULLY determine sleep stages from them
if TESTING:
    print(">>> YOU SHOULD ONLY SEE THIS WHEN TESTING <<<<<")
    SPLIT_HR = split_df_5min(HR_DF)
    SPLIT_HR = SPLIT_HR.set_index('startTime')

# In[]:
if ACC:
    # Adds movement results from the
    # accelerometer module to dataframe
    def acc_peaks(heart_rate_df, acceleration_result):   # pragma: no cover
        """
        Updates MovementDiscard flag in HR_df
        based on the accelerometer data
        """
        # generates "tags" to discard
        # HR local peaks generated by
        # movement during sleep
        # and classify wake/sleep
        # by movement information
        heart_rate_df['Movement'] = acceleration_result

        return heart_rate_df


def stage_type(processed_row):
    """
    Generates a decision on
    sleep stage from data
    Outputs:
        3 ==> wake
        2 ==> light
        1 ==> REM
        0 ==> deep
        999 ==> error detection
    """

    hf_norm = processed_row['HF_Norm']
    bpm_avg = processed_row['avgBPM']
    hr_threshold = 45

    # decision boundaries
    # to be empirically determined
    border = [0.075, 0.1, 0.22, 1.5]

    if hf_norm > border[0] and hf_norm < border[1]:
        # REM
        return 1

    # Light/Wake share same HF value range
    elif hf_norm > border[1] and hf_norm < border[2]:
        # Decide based on HR
        if bpm_avg <= hr_threshold:
            # LIGHT SLEEP
            return 2
        else:
            # WAKE
            return 3

    elif hf_norm >= border[2] and hf_norm < border[3]:
        # Deep
        return 0

    else:
        # error nan value for debug & test
        return  # 999 <--nan is better, can be b/ffilled


if TESTING:
    SPLIT_HR['Stage'] = SPLIT_HR.apply(stage_type, axis=1)
    SPLIT_HR['Stage'] = SPLIT_HR['Stage'].bfill()
    SPLIT_HR['Stage'] = SPLIT_HR['Stage'].ffill()


# In[]:
def process_hr_data(acc_measurements, hr_measurements):
    """
    collates all functions in a format
    that's easier to follow through
    """
    if TESTING:
        # read in the csv
        print(".csv read-in in wrapper function")
        hr_initial = pd.read_csv(CSVFILE)
        hr_initial['Timestamp'] = pd.to_datetime(hr_initial['Timestamp'])
        hr_initial.set_index('Timestamp', inplace=True)
        hr_dataframe = hr_initial
    else:
        # read in the raw file
        print("HR >> Raw measurement read-in")
        hr_dataframe = construct_df(hr_measurements)

    hr_dataframe = resample_4hz(hr_dataframe)
    hr_dataframe = split_df_5min(hr_dataframe)
    hr_dataframe.set_index('startTime', inplace=True)

    if ACC:  # do we also use acc data?
        # added for dimension optimization
        while len(hr_dataframe) > len(acc_measurements):
            last_row = len(hr_dataframe) - 1
            hr_dataframe = hr_dataframe.drop(hr_dataframe.index[last_row])
        hr_dataframe = acc_peaks(hr_dataframe, acc_measurements)

    hr_dataframe['Stage'] = hr_dataframe.apply(stage_type, axis=1)

    # fill all nan values from neighbors
    hr_dataframe['Stage'] = hr_dataframe['Stage'].bfill()
    hr_dataframe['Stage'] = hr_dataframe['Stage'].ffill()
    # [x] This should've been replaced with a function that
    # checked neighboring windows and came to a decision based
    # on those values. However, indexing neighbors with pandas
    # proved much more difficult than expected and significantly
    # slowed processing speeds (at least in my attempt at it.)

    return hr_dataframe


# In[]:
# check if function wrapper
# does its job as expected
if TESTING:
    HR_PROCESSED_AT_ONCE = process_hr_data(np.zeros(46), np.zeros(46))

# do visual check on data
if PLOT:
    def plot_stuff():   # pragma: no cover
        """
        debugging function, used to visualise
        the HR and RR between start and stop indices
        """
        fig1 = plt.figure()
        fig1.suptitle("HR Metrics")

        ax1 = fig1.add_subplot(221)
        ax1.plot(HRMEAS['BPM'], 'k', label='BPM')
        ax1.plot(HRMEAS['RR'], 'r', label='RR')
        ax1.set_title("BPM and RR")
        ax1.legend()

        ax2 = fig1.add_subplot(222)
        ax2.hist(HRMEAS['RR'])
        ax2.set_title("RR Histogram")

        ax3 = fig1.add_subplot(223)
        ax3.plot(SPLIT_HR['HF_Norm'], 'k', label='HF')
        ax3.plot(SPLIT_HR['LF_Norm'], 'r', label='LF')
        ax3.set_title("LF and HF")
        ax3.legend()

        ax4 = fig1.add_subplot(224)
        ax4.plot(SPLIT_HR['LF_HF'], 'r', label='LF/HF')
        ax4.plot(SPLIT_HR['LF_VLF_Norm'], 'go',
                 label='LF/VLF')
        ax4.set_title("Ratios, LF/VLF=0 bc TWEAK param")
        ax4.legend()

        plt.show()

    plot_stuff()


# In[]:
def pairwise(iterable):   # pragma: no cover
    """
    s -> (s0,s1), (s1,s2), (s2, s3), ...
    """
    one, two = tee(iterable)
    next(two, None)
    return zip(one, two)

# In[]:


EPOCH = datetime.datetime.utcfromtimestamp(0)


def convert_time(date_time_obj):   # pragma: no cover
    """
    converts datetime object
    back to timestamp format
    """
    return (date_time_obj - EPOCH).total_seconds() * 1000.0


# In[]:
def make_my_day(interp_df,
                output_name='database.csv'):
    """
    This should generate a row of
    compact info to be appended to
    a csv database, for visualising
    results into the GUI.
    """

    # get count of stages to compute sleep score
    date = interp_df.index[0]

    stage_cnt = interp_df.groupby('Stage').count()['HF_Norm'].tolist()

    # corner cases in case not enoguh
    # stage types detected
    # so that sleepscore doesn't divide by 0
    if len(stage_cnt) == 2:
        stage_cnt.append(-1)

    if len(stage_cnt) == 3:
        stage_cnt.append(-1)

    # these values are 'fixed'
    # in such a way that sleepscore
    # will be negative
    if len(stage_cnt) < 2:
        stage_cnt = [0, 1, -1, 0]

    # find start/stop time for sleep
    # for i_cant_use_pandas in range (len(interpreted_df)):
    #    index = i_cant_use_pandas

    # we're gonna need total sleep time (in hours)
    sleep_hrs = sum(stage_cnt[:-1]) * 5 / 60

    # restlessness index
    restlessness = 0
    for index, row in interp_df.iterrows():
        # print(index, row['Stage'])
        if (row['Stage'] != 0) and (row['Movement'] == 1):
            restlessness += 5 / 60

    # expressed as fraction of total sleep time
    if sleep_hrs > 0:
        restlessness = restlessness / sleep_hrs * 100
    else:  # if no sleep at all --> 100% restless
        restlessness = 100

    # SCORE = time/8 * (deep+REM / light)
    score = (sleep_hrs / 8) * ((stage_cnt[0]+stage_cnt[1])/stage_cnt[2])

    stages, start_times, stop_times = [], [], []

    for (idx1, row1), (idx2, row2) in pairwise(interp_df.iterrows()):
        stages.append(row1['Stage'])
        start_times.append(idx1.strftime('%Y%m%d%H%M'))
        stop_times.append(idx2.strftime('%Y%m%d%H%M'))

    stages = [int(stage) for stage in stages]

    daily_summary = pd.DataFrame(columns=['Date',
                                          'Score',
                                          'Total_Time',
                                          'Restlessness',
                                          'Stage_Start',
                                          'Stage_Stop',
                                          'Stage_Type'])

    daily_summary = daily_summary.append({'Date': date.strftime('%Y%m%d%H%M'),
                                          'Score': score,
                                          'Total_Time': sleep_hrs,
                                          'Restlessness': restlessness,
                                          'Stage_Start': str(start_times),
                                          'Stage_Stop': str(stop_times),
                                          'Stage_Type': str(stages)},
                                         ignore_index=True)

    if not os.path.isfile(output_name):
        daily_summary.to_csv(output_name,
                             header=list(daily_summary),
                             index=False)

    else:  # else it exists so append without writing the header
        daily_summary.to_csv(output_name, mode='a',
                             header=False, index=False)

    return daily_summary

if TESTING:
    DISP_DAY = make_my_day(HR_PROCESSED_AT_ONCE,
                           output_name='test_db.csv')
