# -*- coding: utf-8 -*-
"""
Created on Thu Jun  7 20:03:08 2018

@author: Berkay
"""
import unittest
import math
import numpy as np
from scipy.signal import butter, filtfilt
import dataparse.data_parser as dp
import accelerometer.calculator_angle as calc

S_1 = [1, 4, 3]
S_2 = [6, 7, 7]
S_3 = [3, 4, 5]
S_4 = [5, 6, 2]
S_5 = [5, 4, 1]
DATA_TEST = np.concatenate([S_1, S_2, S_3, S_4, S_5], axis=0)
MEAS_VAL = dp.parse_func("files/2018-06-26T09:16")
HR_MEAS = MEAS_VAL[1]
ACC_MEAS = MEAS_VAL[0]
ANG_ARR = calc.total_result(ACC_MEAS, HR_MEAS)


class Test(unittest.TestCase):
    """Test class"""

    def test_formula(self):
        """tests the formula for constant values"""
        self.assertEqual(45, calc.calc_angle(3, 4, 5))

    def test_parameters(self):
        """tests the formula for different variables"""
        formula = math.atan(7.4/(math.sqrt(4.4**2+3**2)))*180/math.pi
        self.assertEqual(formula, calc.calc_angle(4.4, 3, 7.4))

    def test_average(self):
        """test for computing the average value of x,y and z coordinates for a
        given time interval"""
        self.assertEqual(np.all(calc.calc_avr(DATA_TEST)), np.all([4, 5,\
                         18./5]))

    def test_angle(self):
        """test for finding the angle change"""
        a_test = 40
        b_test = 25
        self.assertLess(calc.change_angle(a_test, b_test), abs(36))

    def test_filtering(self):
        """test Butterworth filtering"""
        b_num, a_denom = butter(2, 0.3, 'low', analog=False)
        y_test = filtfilt(b_num, a_denom, DATA_TEST)
        self.assertEqual(np.all(y_test), np.all(calc.butterworth_filter(0.3,\
                         4, DATA_TEST)))

    def test_transform(self):
        """test for adjusting the array size"""
        self.assertTrue((len(calc.transform_array(ACC_MEAS,\
                         HR_MEAS)) % len(HR_MEAS)) == 0)

    def test_reshape_average(self):
        """test for reshaping the array and finding the
        average of every nth value"""
        test_array = ([4.2, 4.8, 3.6])
        self.assertEqual(np.all(test_array),\
                         np.all(calc.reshape_and_average(DATA_TEST, S_1)[0]))

    def test_movement(self):
        """testing if there is a movement"""
        self.assertEqual('0', calc.is_there_movement(ANG_ARR[1]))

if __name__ == '__main__':
    unittest.main()
