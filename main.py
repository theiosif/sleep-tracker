#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  2 02:06:32 2018

@author: KAAN
"""

import database_creater as db
from py_interface.py_interface import start_interface

db.create()
start_interface()