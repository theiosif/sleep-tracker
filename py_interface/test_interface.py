# -*- coding: utf-8 -*-
"""
Created on Sun Jul  8 21:09:23 2018

@author: Eftihia
"""

from tkinter import *
import unittest
import py_interface.py_interface as gui

class TKinterTestCase(unittest.TestCase):
        def testClassSetup(self):
            """
            Startpage shows up?
            """
            gui.Application()

        #    mock classes

        def testStartFrame(self):
            """
            Test if the StartPage Frame is acessed
            """
            PageName = 'StartPage'
            k = gui.Application()
            result = k.show_frame(StartPage)
            self.assertEqual(StartPage, result)

        def testGraphFrame(self):
            """
            Test if the GraphPage Frame is acessed
            """
            PageName = 'GraphPage'
            k = gui.Application()
            result = k.show_frame(GraphPage)
            self.assertEqual(GraphPage, result)

        def testSleepTimeFrame(self):
            """
            Test if the SleepTime Frame is acessed
            """
            PageName = 'SleepTime'
            k = gui.Application()
            result = k.show_frame(SleepTime)
            self.assertEqual(SleepTime, result)

        def testSleepScoresFrame(self):
            """
            Test if the SleepScores Frame is acessed
            """
            PageName = 'SleepScores'
            k = gui.Application()
            result = k.show_frame(SleepScores)
            self.assertEqual(SleepScores, result)

        def testSleepStagesFrame(self):
            """
            Test if the SleepStages Frame is acessed
            """
            PageName = 'SleepStages'
            k = gui.Application()
            result = k.show_frame(SleepStages)
            self.assertEqual(SleepStages, result)

#        def callback(event):
#            print ("clicked at", event.x, event.y)


if __name__ == '__main__':
    unittest.main()