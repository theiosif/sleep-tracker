
# -*- coding: utf-8 -*-
"""
@author: Joseph

We're improvising as we go, and since
the GUI needs input data for faster
development, here's FAKE-MAKER.

Theoretically, the dataframe we''ll end up
using will follow the same structure

Sleep stage is encoded as int:
0 - awake
1 - light
2 - deep
3 - REM (if we even get there)
"""


import random
import time
import numpy as np
import pandas as pd
# PyLint made me do this
# pylint: disable=E1101
# ^^^ This one is bc of fake 'numpy has no XXX member stuff'


def time_split(start_time, end, format_s, prop):
    """Get a time at a proportion of a range of two formatted times.

    start and end should be strings specifying times formated in the
    given format (strftime-style), giving an interval [start, end].
    prop specifies how a proportion of the interval to be taken after
    start.  The returned time will be in the specified format.
    """

    stime = time.mktime(time.strptime(start_time, format_s))
    etime = time.mktime(time.strptime(end, format_s))

    ptime = stime + prop * (etime - stime)

    return time.strftime(format_s, time.localtime(ptime))


def random_hour(start_time, end):
    """
    returns random timestamp
    between given start/end
    timestamps
    """
    return time_split(start_time, end, '%Y%m%d%H%M', random.random())


def fake_db(export=True):
    """""
    exports a .csv file
    of "random" values
    in the same format
    as the real database
    """
    # constants to generate fake database
    num_days = 30
    start_date = "201806"
    hhmm_start = "2030"
    hhmm_stop = "1130"

    start_times_arr = []
    stop_times_arr = []

    for day in range(1, num_days):
        if day+1 > 9:
            start = start_date + str(day) + hhmm_start
            stop = start_date + str(day) + hhmm_stop
        else:
            start = start_date + "0" + str(day) + hhmm_start
            stop = start_date + "0" + str(day+1) + hhmm_stop
        start_times_arr.append(start)
        stop_times_arr.append(stop)

    # daily timestamps
    # TIMES = pd.date_range("21:30", "21:30", freq="5min")

    # daily sleep score
    sleep_score = np.random.normal(7, 2.5, num_days-1)
    # getMinMax(sleep_score)

    # daily total time spent sleeping
    total_time = np.random.normal(28800, 3600, num_days-1)
    # getMinMax(total_time)

    # daily restlessness (in % out of 100)
    restless = np.random.normal(10, 2, num_days-1)
    # getMinMax(restless)

    # make a random number of sleep stages for every night
    num_stages = np.random.randint(6, 12, size=num_days-1)

    # function to create random timestamp
    # between 2 existing given ones

    start_array = [[]]
    stop_array = [[]]
    stage_type_array = [[]]
    # every day
    for create_day in range(num_days-1):
        start_array_per_day = []
        stop_array_per_day = []
        stage_type_array_per_day = []

        # initiate params for random hour generations
        stage_start = start_times_arr[create_day]
        stage_stop = stop_times_arr[create_day]

        # generate random number of stages
        for create_stage in range(num_stages[create_day]):
            stop = random_hour(stage_start, stage_stop)
            start_array_per_day.append(stage_start)
            stop_array_per_day.append(stage_start)
            stage_start = stop
            stage_type_array_per_day.append(np.random.randint(0, 4))

        start_array.append(start_array_per_day)
        stop_array.append(stop_array_per_day)
        stage_type_array.append(stage_type_array_per_day)

    start_array = start_array[1:]
    stop_array = stop_array[1:]
    stage_type_array = stage_type_array[1:]

    start_stop_type = np.vstack((start_array, stop_array, stage_type_array))

    fake_data = pd.DataFrame(columns=['Date',
                                      'Score',
                                      'Total_Time',
                                      'Restlessness',
                                      'Stage_Start',
                                      'Stage_Stop',
                                      'Stage_Type'])

    for day in range(num_days-1):
        fake_data = fake_data.append({'Date': start_times_arr[day],
                                      'Score': sleep_score[day],
                                      'Total_Time': total_time[day],
                                      'Restlessness': restless[day],
                                      'Stage_Start': start_stop_type[0][day],
                                      'Stage_Stop': start_stop_type[1][day],
                                      'Stage_Type': start_stop_type[2][day]},
                                     ignore_index=True)
    if export:
        fake_data.to_csv('AS_FAKE_AS_IT_CAN_GET.csv', index=False)

    return fake_data

if __name__ == '__main__':
    DB_OUT = fake_db()
