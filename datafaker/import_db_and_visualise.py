# -*- coding: utf-8 -*-
"""
Created on Sat Jun 30 00:47:01 2018

@author: Joseph
"""

# this script was created with the intention to parse
# data for visualising in the GUI. Therefore, it roughly
# translates to what we called "visualisation" in the
# splitting of responsibilites

# In[]:
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

DATABASE = pd.read_csv('AS_FAKE_AS_IT_CAN_GET.csv')
for index, row in DATABASE.iterrows():
    row['Stage_Start'] = row['Stage_Start'].split()
    row['Stage_Stop'] = row['Stage_Stop'].split()

NUM_ENTRIES = len(DATABASE)

# In[]:


def fix_date(date):
    """
    parses formatstring
    to datetime object
    """
    date = str(date)
    date = date[:-4]
    date = pd.to_datetime(date, format="%Y%m%d")
    return date


def fix_times(the_problem):
    """
    parses formatstring
    to datetime object
    """
    # we want a uniform format on all strings
    the_problem = the_problem[1:-1]+','
    the_problem = the_problem.split()
    the_problem = pd.to_datetime(the_problem, format="'%Y%m%d%H%M',")

    the_solution = the_problem
    return the_solution


def fix_list(strings_list):
    """
    parses string back
    to list format
    """
    strings_list = strings_list[1:-1]
    strings_list = strings_list.split(', ')
    int_list = list(map(int, strings_list))
    return int_list


# In[]:


DATABASE['Date'] = [fix_date(x) for x in DATABASE['Date']]
DATABASE['Stage_Start'] = [fix_times(x) for x in DATABASE['Stage_Start']]
DATABASE['Stage_Stop'] = [fix_times(x) for x in DATABASE['Stage_Stop']]
DATABASE['Stage_Type'] = [fix_list(x) for x in DATABASE['Stage_Type']]
DATABASE['Total_Time'] = DATABASE['Total_Time']/3600

# In[]:


# Plot out sleep stage evolution
# DATA IS BAD THAT'S WHY IT LOOKS BAD
def plot_sleep_stages(day):
    """
    plots sleep stages
    of given day
    """
    plt.figure()
    day_name = day['Date'].strftime('%Y-%m-%d')
    plt.suptitle('Sleep Stages on {}'.format(day_name))
    plt.plot(day['Stage_Start'], day['Stage_Type'], 'r-o')
    plt.yticks(np.arange(4), ['Deep', 'REM', 'Light', 'Wake'])
    plt.show()

# plot out 2 days as proof of concept
for index, row in DATABASE.iterrows():
    if index < 2:
        plot_sleep_stages(row)


# In[]:
# Print sleep quality evolution based on score
def plot_sleep_scores(database):
    """
    plots sleep scores
    over database range
    """
    plt.figure()

    day_start = database['Date'][0].strftime('%Y-%m-%d')
    day_stop = database['Date'][NUM_ENTRIES-1].strftime('%Y-%m-%d')
    plt.suptitle('Sleep Quality from {} to {}'.format(day_start, day_stop))

    plt.xlabel('Date')
    plt.ylabel('Sleep Quality')

    plt.plot(DATABASE['Date'], DATABASE['Score'], 'r-o')
    plt.show()

plot_sleep_scores(DATABASE)


# In[]:
# Print total sleep time
def plot_sleep_time(database):
    """
    plots sleep times
    over database range
    """
    plt.figure()

    day_start = database['Date'][0].strftime('%Y-%m-%d')
    day_stop = database['Date'][NUM_ENTRIES-1].strftime('%Y-%m-%d')

    plt.suptitle('Total Sleep Time (hrs) from {} to {}'
                 .format(day_start, day_stop))

    plt.xlabel('Date')
    plt.ylabel('Sleep Time')

    plt.plot(database['Date'], database['Total_Time'], 'g-v')
    plt.show()

plot_sleep_time(DATABASE)
