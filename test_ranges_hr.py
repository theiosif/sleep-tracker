# -*- coding: utf-8 -*-
"""
Created on Sun Jun  24 20:03:08 2018

[!] THIS SCRIPT IS NOT A DUPLICATE OF c/heartrate/test_ranges.py
This one tests from raw files.
The other tests from a csv read-in.
Both, however, provide the same tests

@author: Joseph
"""
import unittest
import pandas as pd
import numpy as np
from accelerometer.calculator_angle import total_result as acc

if __name__ == '__main__':
    import heartrate as hr
    from heartrate import process_hr_data as pro_hr
    from data_parser import parse_func as parse
else:
    import heartrate.heartrate as hr
    from heartrate.heartrate import process_hr_data as pro_hr
    from dataparse.data_parser import parse_func as parse


TESTFILE = "./files/processed/2018-06-16T07:39"
file = open(TESTFILE, "rb")

parsed = parse(TESTFILE)

HR_INITIAL = hr.construct_df(parsed[1])
HR_RESAMPLED = hr.resample_4hz(HR_INITIAL)
HR_SPLIT = hr.split_df_5min(HR_RESAMPLED)
HR_SPLIT.set_index('startTime', inplace=True)

PRO_ACC = list(acc(parsed[0], parsed[1]))
PRO_ACC[0] = pd.to_datetime(PRO_ACC[0], unit='ms')

# catch data at different
# stages of processing


PRO = pro_hr(PRO_ACC[1], parsed[1])


# In[]:

class Test(unittest.TestCase):
    """
    provides boundary checks for
    the processed values of the
    heartrate data
    """
    def test_rr_formula(self):
        """
        [raw]Tests the formula for constant values
        """
        heartrate = 80
        self.assertEqual(750, hr.hr_to_rr_milisec(heartrate))

    def test_average_hr(self):
        """
        [raw]Make sure user isn't dead/ sensor malfunctions
        """
        average_heartrate = PRO.avgBPM.mean()
        self.assertTrue((average_heartrate <= 120) and
                        (average_heartrate >= 35))

    def test_resample(self):
        """
        [raw]4hz resampling successful?
        """
        get_indexes = HR_RESAMPLED.index.values
        diffs = [x - get_indexes[i - 1]
                 for i, x in enumerate(get_indexes)][1:]
        check = diffs.count(diffs[0]) == len(diffs)
        four_hertz = np.timedelta64(250, 'ms')

        self.assertTrue(check and (four_hertz == diffs[0]))

    def test_split(self):
        """
        [raw]Splitting dataframe in 5min chunks?
        """
        subunit = len(HR_SPLIT)/len(HR_RESAMPLED) * 1200
        self.assertTrue(subunit < 1)

    def test_freq_params(self):
        """
        [raw]Verify if freq analysis returns values in expected range
        """
        check = True

        for index, row in PRO.iterrows():
            if row['HF_Norm'] >= 1:
                check = False
            if row['LF_HF'] <= 0.2:
                check = False
            if row['LF_Norm'] >= 1:
                check = False

        self.assertTrue(check)

    def test_stages_nan(self):
        """
        [raw]Column fill success?
        """

        self.assertFalse(PRO['Stage'].isnull().values.any())

    def test_stage(self):
        """
        [raw]Statically test sleep stages.
        """

        st_test = pd.DataFrame({'HF_Norm': [0.076, 0.15, 0.15, 0.4, 0.074],
                                'avgBPM': [42, 42, 50, 50, 64]})

        st_test['Stage'] = st_test.apply(hr.stage_type, axis=1)
        check = st_test['Stage'].tolist()

        boolCheck = (check[0] == 1 and
                     check[1] == 2 and
                     check[2] == 3 and
                     check[3] == 0 and
                     np.isnan(check[4]))

        self.assertTrue(boolCheck)

    def test_colnames(self):
        """
        [raw]Exported column names ok?
        """
        export_df = hr.make_my_day(PRO, output_name='test_db.csv')
        colnames = ['Date',
                    'Score',
                    'Total_Time',
                    'Restlessness',
                    'Stage_Start',
                    'Stage_Stop',
                    'Stage_Type']

        self.assertTrue((export_df.columns.values == colnames).all())

if __name__ == '__main__':
    unittest.main()
