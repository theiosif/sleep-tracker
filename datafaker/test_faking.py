# -*- coding: utf-8 -*-
"""
Created on Sun Jun  24 20:03:08 2018

[!] THIS SCRIPT IS NOT A DUPLICATE OF c/heartrate/test_ranges.py
This one tests from raw files.
The other tests from a csv read-in.
Both, however, provide the same tests

@author: Joseph
"""
import unittest
import datafaker.fake_db_maker as fk
TESTFILE = "./datafaker/AS_FAKE_AS_IT_CAN_GET.csv"
FAKE_DF = fk.fake_db(export=False)

# In[]:


class Test(unittest.TestCase):
    """
    provides boundary checks for
    the processed values of the
    heartrate data
    """
    def test_colnames(self):
        """
        [db_faker]Output colnames corresponding?
        """
        colnames = ['Date',
                    'Score',
                    'Total_Time',
                    'Restlessness',
                    'Stage_Start',
                    'Stage_Stop',
                    'Stage_Type']

        self.assertTrue((FAKE_DF.columns.values == colnames).all())

    def test_length(self):
        """
        [db_faker]29 days in fake db?
        """

        length = len(FAKE_DF)

        self.assertEqual(29, length)


if __name__ == '__main__':
    unittest.main()
