#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  2 02:06:32 2018

@author: KAAN
"""

import os
from dataparse.data_parser import parse_func as parse
from accelerometer.calculator_angle import total_result as acc
from heartrate import heartrate as hr


def create():
    '''By combaning the data processing scripts creates database'''
    file_content = os.listdir("files")
    os.chdir("files")
    file_content.sort()
    file_content.remove('.gitkeep')
    file_content.remove('processed')

    for file in file_content:
        print('Processing File: ', file)
        parsed = parse(file)

        # Filter the raw data file according to their size of
        # Heartrate data
        if len(parsed[1]) > 100:
            pro_acc = acc(parsed[0], parsed[1])
            processed = hr.process_hr_data(pro_acc[1], parsed[1])
            os.chdir("..")
            hr.make_my_day(processed)
            os.chdir("files")
        os.rename(file, 'processed/'+file)
    os.chdir("..")

    return
