#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 15 15:29:13 2018

@author: KAAN
"""

import unittest
from dataparse.data_parser import parse_func as parse

FILE = "./dataparse/test_file"


class Test(unittest.TestCase):
    '''This class test the parse_func works properly or not'''

    def test_parse_func_accelerometer(self):
        '''Checks the accelerometer measurements'''
        time_stamp = 1528205748602
        payload_x = 0.6963114142417908
        payload_y = 4.515256404876709
        payload_z = 8.755337715148926
        exp_meas = (time_stamp, payload_x, payload_y, payload_z)
        accelero = parse(FILE)[0][0]
        self.assertEqual(exp_meas, accelero)

    def test_parse_func_heart_rate(self):
        '''Checks the heart rate measurements'''
        time_stamp = 1528219237197
        payload = 107.0
        exp_meas = (time_stamp, payload)
        heart_rate = parse(FILE)[1][0]
        self.assertEqual(exp_meas, heart_rate)


if __name__ == '__main__':
    unittest.main()
