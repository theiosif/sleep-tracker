#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 26 23:18:36 2018

@author: KAAN
"""

import struct


def parse_func(filename):
    """Takes file and returns timestamps and payloads."""
    # Reading binary data from file
    file = open(filename, "rb")

    data = file.read()
    file.close()

    # Seperating related data for each
    # measurement according to their sensor type
    # and convert binary datas to related values
    acc_meas = []
    hr_meas = []
    s_type = 8
    # iterate until running out of bytes to read
    while s_type < len(data):
        # accelerometer(0) data type
        if data[s_type] == 0:
            acc_meas.append(
                struct.unpack('>Qfff',
                              data[s_type - 8: s_type] +
                              data[s_type + 1: s_type + 13]))
            s_type += 21
        # Heart Rate(2) data type
        elif data[s_type] == 2:
            hr_meas.append(
                struct.unpack('>Qf',
                              data[s_type - 8: s_type] +
                              data[s_type + 1: s_type + 5]))
            s_type += 13
        elif data[s_type] == 1:
            s_type += 13

    return (acc_meas, hr_meas)
