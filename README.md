# Sleep Tracking with Wearables
This software is meant to process and interpret raw data from a Huawei Watch 2.   
All results will be available to the user via a graphical interface.

## Functionality
Collated in the GUI are the following features:
1. **[Check for new files]**
Checks the ``` /files```  folder for new files. If any new files are present,   
they are parsed, added to ```database.csv``` and moved to ``` files/processed```   
so that they will not become duplicate database entries.
    
2. **[Graph page]**
Provides overview of the following graphs:
*Sleep Stages*,  *Sleep Scores*, *Sleep Times*
    
3. **[Exit]**
Makes the application close.

## Usage
```virtualenv``` apparenty has some issues with the ```--relocatable``` option. 

In case portability issues arise, below are listed the commands which    
got our program running on a fresh Ubuntu install:
```
$ sudo apt-get install python3-venv
$ python3 -m venv virtualenv

$ source virtualenv/bin/activate
$ pip install --upgrade pip
$ python3 setup.py install
$ pip install -r Pasithea.egg-info/requires.txt 
$ pip install requests

$ source virtualenv/bin/activate   
$ python3 main.py
```
## Project Structure, with authors
```
c-master
│   README.md
│   main.py  # (Kaan & Joseph)    
│   database_creater.py  # (Kaan, path fix by Joseph)    
│
│# Documentation folder
└───docs
│   │   projektplan.md
│   │   ProjectOverview.png  # (Joseph)
│
│# DATA PROCESSING FOR ACCELERATION
└───accelerometer
│   │   calculator_angle.py  # (Berkay)
│
│
│# UTILITY MODULE, NO INTEGRATED FUNCTIONALITY
│# Functions from the 2nd script used to visualise data
│# were copy-pasted into GUI as button functions
└───datafaker  
│   │   fake_db_maker.py  #(Joseph)
│   │   import_db_and_visualise.py  #(Joseph)
│   
│ 
│ # RAW FILE READ-IN
└───dataparse
│   │   data_parser.py  #(Kaan)
│   
│   
│ # DATA PROCESSING FOR HEARTRATE
└───heartrate
│   │   heartrate.py  #(Joseph)
│   
│
│ # GRAPHICAL USER INTERFACE
└───py_interface
│   │   py_interface.py   # (Efia)   
│   │   img.gif   # (Joseph)   
```

## References for Medical Backgroud
Sleep Stage Assessment Using Power Spectral Indices of Heart Rate Variability With a Simple Algorithm, Keiko Tanida, Masashi Shibata, Margaret M. Heitkemper   

Spectral Analysis of Heart Rate Variability in Sleep, P. Busek, J. Vankova, J. Opavsky, J. Salinger, S. Nevsimalova