# -*- coding: utf-8 -*-
"""
Created on Sun Jun  24 20:03:08 2018

[!] THIS SCRIPT IS NOT A DUPLICATE OF c/test_ranges_hr.py
This one tests from csv files.
The other tests from a raw file read-in.
Both, however, provide the same tests

@author: Joseph
"""
import unittest
import pandas as pd
import numpy as np
if __name__ == '__main__':
    import heartrate as hr
else:
    import heartrate.heartrate as hr

# update testing flag so
# we may use script output
hr.TESTING = True
if __name__ == '__main__':
    hr.CSVFILE = "2018-06-16T07_39_irregular.csv"
else:
    hr.CSVFILE = "heartrate/2018-06-16T07_39_irregular.csv"

# In[]:
# catch data at different stages of processing
# this is unwrapped code from the process_hr_data collater
HR_INITIAL = pd.read_csv(hr.CSVFILE)
HR_INITIAL['Timestamp'] = pd.to_datetime(HR_INITIAL['Timestamp'])
HR_INITIAL.set_index('Timestamp', inplace=True)

HR_RESAMPLED = hr.resample_4hz(HR_INITIAL)
HR_SPLIT = hr.split_df_5min(HR_RESAMPLED)
HR_SPLIT.set_index('startTime', inplace=True)

PRO = hr.process_hr_data([0]*46, [0]*46)
# In[]:


class Test(unittest.TestCase):
    """
    provides boundary checks for
    the processed values of the
    heartrate data
    """
    def test_rr_formula(self):
        """
        [csv]Tests the formula for constant values
        """
        heartrate = 80
        self.assertEqual(750, hr.hr_to_rr_milisec(heartrate))

    def test_average_hr(self):
        """
        [csv]Make sure user isn't dead/ sensor malfunctions
        """
        average_heartrate = PRO.avgBPM.mean()
        self.assertTrue((average_heartrate <= 120) and
                        (average_heartrate >= 35))

    def test_resample(self):
        """
        [csv]4hz resampling successful?
        """
        get_indexes = HR_RESAMPLED.index.values
        diffs = [x - get_indexes[i - 1]
                 for i, x in enumerate(get_indexes)][1:]
        check = diffs.count(diffs[0]) == len(diffs)
        four_hertz = np.timedelta64(250, 'ms')

        self.assertTrue(check and (four_hertz == diffs[0]))

    def test_split(self):
        """
        [csv]Splitting dataframe in 5min chunks?
        """
        subunit = len(HR_SPLIT)/len(HR_RESAMPLED) * 1200
        self.assertTrue(subunit < 1)

    def test_freq_params(self):
        """
        [csv]Verify if freq analysis returns values in expected range
        """
        check = True

        for index, row in PRO.iterrows():
            if row['HF_Norm'] >= 1:
                check = False
            if row['LF_HF'] <= 0.2:
                check = False
            if row['LF_Norm'] >= 1:
                check = False

        self.assertTrue(check)

    def test_stages_nan(self):
        """
        [csv]Column fill success?
        """

        self.assertFalse(PRO['Stage'].isnull().values.any())

    def test_stage(self):
        """
        [csv]Statically test sleep stages.
        """

        st_test = pd.DataFrame({'HF_Norm': [0.076, 0.15, 0.15, 0.4, 0.074],
                                'avgBPM': [42, 42, 50, 50, 64]})

        st_test['Stage'] = st_test.apply(hr.stage_type, axis=1)
        check = st_test['Stage'].tolist()

        boolCheck = (check[0] == 1 and
                     check[1] == 2 and
                     check[2] == 3 and
                     check[3] == 0 and
                     np.isnan(check[4]))

        self.assertTrue(boolCheck)

    def test_colnames(self):
        """
        [csv]Exported column names ok?
        """
        export_df = hr.make_my_day(PRO, output_name='test_db.csv')
        colnames = ['Date',
                    'Score',
                    'Total_Time',
                    'Restlessness',
                    'Stage_Start',
                    'Stage_Stop',
                    'Stage_Type']

        self.assertTrue((export_df.columns.values == colnames).all())

if __name__ == '__main__':
    unittest.main()
